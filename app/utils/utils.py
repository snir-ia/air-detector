from subprocess import PIPE, Popen

import cv2

from app.utils.logger import logger
from app.utils.config import get_config

config = get_config()


class CommunicablePopen(Popen):
    def __init__(self, cmd, bufsize=-1, executable=None,
                 stdin=None, stdout=PIPE, stderr=PIPE,
                 preexec_fn=None, close_fds=True,
                 shell=False, cwd=None, env=None, universal_newlines=None,
                 startupinfo=None, creationflags=0,
                 restore_signals=True, start_new_session=False,
                 pass_fds=(), **kwargs):
        super().__init__(cmd, bufsize=bufsize, executable=executable,
                         stdin=stdin, stdout=stdout, stderr=stderr,
                         preexec_fn=preexec_fn, close_fds=close_fds,
                         shell=shell, cwd=cwd, env=env, universal_newlines=universal_newlines,
                         startupinfo=startupinfo, creationflags=creationflags,
                         restore_signals=restore_signals, start_new_session=start_new_session,
                         pass_fds=pass_fds, **kwargs)

    def communicate(self, input=None, timeout=None):
        out, err = super().communicate(input, timeout)
        out = out.decode()[:-1]
        err = err.decode()[:-1]
        return out, err


def to_floats(lst):
    return [to_floats(x) if isinstance(x, (tuple, list)) else float(x) for x in lst]


def dump_frame(frame, path):
    path.parent.mkdir(parents=True, exist_ok=True)
    try:
        cv2.imwrite(str(path), frame)
        logger.debug(f'Dumped image {path}')
    except Exception as e:
        logger.error(f'Failed to dump image {path}: {e}')


def get_nested(dic_or_list, keys, default=None):
    result = dic_or_list
    for key in keys:
        try:
            result = result[key]
        except (IndexError, KeyError):
            return default

    return result
