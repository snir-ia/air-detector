import sys
if sys.argv[-1] in ('turnaround-detectors', 'pushback-detectors', 'aggregator'):
    # Patch as early as possible
    # But only patch if using concurrency
    from gevent import monkey
    monkey.patch_all()
from datetime import datetime, timedelta
from time import sleep

import celery.signals
import click

from ia_utils.logging.logging import init_logger

from app.utils.config import get_config
from app.utils.logger import logger
from setup import version

config = get_config()

queues_switch = {
    'turnaround-detectors': 'turnaround-inference',
    'pushback-detectors': 'pushback-inference'
}
queue_name = f'{queues_switch.get(sys.argv[-1], sys.argv[-1])}-queue'.upper().replace('-', '_')
base_worker_params = dict(
    hostname=sys.argv[-1],
    queues=config['SITE_UNIT_RABBITMQ_QUEUES'].get(queue_name, ''),
    task_events=True,
    loglevel='WARNING',
)


@celery.signals.after_setup_logger.connect
def setup_loggers(*args, **kwargs):
    init_logger(service='air-detect', **config['LOGS'])


@click.group()
def main():
    pass


@main.command()
def load_models():
    from app.utils.detectors import (
        get_keypoints_detector,
        get_ramp_objects_detector,
        get_vest_classifier,
        get_pushback_detector,
        get_descriptor_extractor,
        get_object_trackers,
    )

    try:
        extractor = get_descriptor_extractor()
        vest_classifier = get_vest_classifier()
        keypoints_detector = get_keypoints_detector()
        ramp_detector = get_ramp_objects_detector()
        pushback_detector = get_pushback_detector()
        trackers = get_object_trackers()

    except Exception:
        logger.exception('Failed to initialize turnaround detectors.')
        exit(100)


@main.command()
def validate_services():
    from ia_utils.config.validate_rabbitmq_connection import validate_rabbitmq_connection
    from app.services.celery_apps import unit_celery_app, hub_celery_app

    validate_rabbitmq_connection(unit_celery_app)
    validate_rabbitmq_connection(hub_celery_app)


@main.command()
def turnaround_detectors():
    from app.tasks.turnaround_inference import unit_celery_app
    base_worker_params.update(
        pool_cls='gevent',
    )
    worker = unit_celery_app.Worker(**base_worker_params)
    worker.start()


#todo: in the future
# @main.command()
# def pushback_detectors():
#     from app.tasks.pushback_inference import unit_celery_app
#     base_worker_params.update(
#         pool_cls='gevent',
#     )
#     worker = unit_celery_app.Worker(**base_worker_params)
#     worker.start()


if __name__ == '__main__':
    init_logger(service='air-detect', **config['LOGS'])
    logger.info(f"Running {sys.argv[-1]} on air-watchers version {version}")
    main()
