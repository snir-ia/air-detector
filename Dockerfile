ARG DOCKER_REGISTRY
ARG IACVCORE_VERSION="0.5.25"

FROM ${DOCKER_REGISTRY}/iacvcore:${IACVCORE_VERSION}

ARG ARTIFACTORY_URL
ARG ARTIFACTORY_USER
ARG ARTIFACTORY_API_KEY
ARG ARTIFACTORY_PYPI_URL=https://${ARTIFACTORY_USER}:${ARTIFACTORY_API_KEY}@${ARTIFACTORY_URL}/artifactory/api/pypi/pypi-local/simple

WORKDIR /app

#RUN groupadd -g 3000 -r air && useradd -u 3000 -r -s /bin/false -g air air
#RUN mkdir /var/air && chown -R air:air /app /var/air && chown -R air:air /iacvcore/models

COPY requirements/debian requirements/debian
RUN apt update \
    && apt install -y $(cat requirements/debian) \
    && apt -o Dpkg::Options::="--force-confmiss" install --reinstall netbase

COPY requirements/core.txt requirements/core.txt
RUN cat requirements/core.txt | grep -v "# " | xargs -L 1 pip install --extra-index-url=$ARTIFACTORY_PYPI_URL

COPY requirements/ia.txt requirements/ia.txt
RUN pip install -r requirements/ia.txt --extra-index-url=$ARTIFACTORY_PYPI_URL

COPY . .

RUN ln -s /app/startup /usr/local/bin/air

# WATCHERS SPECIFIC
#USER air

CMD ["air"]
