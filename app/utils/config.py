import pathlib

from ia_utils.config import make_get_config, ConfigError

config_default_values = {
    "AIRPORT_IATA": "TLV",
    "APP_HOME_DIR": "/var/air",
    "APP_QUEUE": "air-watchers-queue",
    "CAMERA_IMAGES_PATH": "tmp_images",
    "MONITORED_ZONE_MOVING_AVG": 25,
    "MONGODB_URI": "mongodb://user:dds7bksad82hdldj3@mongodb:27017/air_mongo",
    "TURNAROUND_INFERENCE_WORKERS": 3,
    "PUSHBACK_INFERENCE_WORKERS": 3,
    "TURNAROUND_CHECK_INTERVAL_MS": 500,
    "MONITORING_SECONDS_INTERVAL": 60,

    "SITE_UNIT_TOKEN": "token",

    "FIDS_API": {
      "PASSWORD": "",
      "URL": "http://host.docker.internal:4052/fids",
      "USER": "",
      "TYPE": "Mock",
      "API_KEY": ""
    },

    "JOB_MANAGER": {"HOST": "0.0.0.0", "PORT": 7100},

    "SITE_MEMO_REDIS": "redis://:password@redis:6379/2",

    "SITE_UNIT_RABBITMQ": {
      "broker": "amqp://user:password@air_rabbitmq:5672",
      "broker_use_ssl": None
    },

    "HUB_RABBITMQ": {
      "broker": "amqp://user:password@hub_rabbitmq:5672",
      "broker_use_ssl": None
    },

    "HUB_RABBITMQ_QUEUES": {
        "RPC_QUEUE": "hub_rpc"
    },

    "EVENT_PROCESSING_EXCHANGE": "event_processing",
    "SITE_UNIT_RABBITMQ_QUEUES": {
        "AIR_SCHEDULER_QUEUE": "scheduler_queue",
        "SAFETY_WRAPPER_QUEUE": "safety_queue",
        "STATUS_UPDATE_QUEUE": "status_update_queue",
        "MANAGER_QUEUE": "manager_queue",
        "AGGREGATOR_QUEUE": "aggregator_queue",
        "CAMERA_MOVEMENT_QUEUE": "camera_movement_queue",
        "CAMERA_PARAMS_QUEUE": "camera_params_queue",
        "TURNAROUND_INFERENCE_QUEUE": "turnaround_inference_queue",
        "PUSHBACK_INFERENCE_QUEUE": "pushback_inference_queue"
    },

    "LOGS": {
      "console_level": "INFO",
      "elastic_config": {
        "auth_details": [
          "elastic",
          "pAU4kqL4WT191W0t999n4FYQ"
        ],
        "auth_type": "BASIC_AUTH",
        "es_additional_fields": {
          "environment": "dev",
          "system_code": "docker"
        },
        "hosts": [
          {
            "host": "localhost", 
            "port": 9200
          }
        ]
      },
      "handlers": "console"
    },
    "UPDATE_COMMON_DATA_INTERVAL" : 60,
    "PROCESS_RATE": {
      "PUSHBACK": 1,
      "SCANNER": 10,
      "TURNAROUND": 3
    },
    "DEBUGGING": {
      "DEBUG_CAMERA": 10,
      "ENABLED": False,
      "IMAGE_PATH": "/var/air/debug_img.jpg",
      "NOT_RTSP": True
    },
    "MODELS_CONFIG": {
      "keypoints_model": "hrnet",
      "ramp": {
      "image_max_area": 2073600,
      "image_min_area": 32400,
      "init_subclasses": True,
      "subclasses": {
        "aircraft": None,
        "object": "ramp_subclasses_object_2022_01_04_mobilenetv3.pth.tar",
        "person": "ramp_subclasses_person_2021_10_01.pth.tar",
        "structure": "ramp_subclasses_structure_2022_01_04_mobilenetv3.pth.tar",
        "vehicle": "ramp_subclasses_vehicle_2022_01_04_mobilenetv3.pth.tar"
      },
      "subclasses_batch_size": 16,
      "subclasses_crop_size": 224,
      "subclasses_min_confidence": 0.5,
      "subclasses_resize_size": 256,
      "superclasses": {
        "checkpoint": "ramp_superclasses_checkpoint_2022_01_11_vfnet.pth",
        "conf_threshold": 0.5,
        "config": "ramp_superclasses_config_2022_01_11_vfnet.py",
        "iou_threshold": 0.25,
        "max_detections": 500,
        "min_size": 8,
        "threshold": 0.3,
        "use_fp16": True
      },
      "use_tensorrt": False
    },
    "tracker_model_file": "ramp_tracker_model_01_29_2021.pth"
  },    
  "PATH_TO_MODELS": "/usr/src/app/models",
  "DATA_RETENTION_DAYS": 3    
  
}

get_config = make_get_config(
    category='air',
    project='air-watchers',
    default_values=config_default_values,
    project_path=pathlib.Path(__file__).parent.parent.parent
)
