FROM iacvcore6

ARG ARTIFACTORY_URL
ARG ARTIFACTORY_USER
ARG ARTIFACTORY_API_KEY
ARG ARTIFACTORY_PYPI_URL=https://alexz:AKCp8ii9C7DPLWU16SA8ruy1WvEFUhm3JQQxpS4BXMknQdkBcWKPn3dQUyQ1ZqzDDdUfL149X@repo.intell-act.com/artifactory/api/pypi/pypi-local/simple

WORKDIR /app
COPY requirements/debian requirements/debian
RUN apt-get update
RUN apt update

#todo: take from requirements/debian file
RUN apt install -y gcc libsm6 libxext6 libxrender-dev libglib2.0-0 libgl1-mesa-glx ffmpeg libpq-dev
RUN apt -o Dpkg::Options::="--force-confmiss" install --reinstall netbase

COPY requirements requirements
RUN pip install -r requirements/ia.txt --extra-index-url=$ARTIFACTORY_PYPI_URL
RUN pip install -r requirements/core.txt --extra-index-url=$ARTIFACTORY_PYPI_URL
RUN pip install /iacvcore/.

#todo: install here?
#RUN pip install git+https://github.com/thomasbrandon/mish-cuda

COPY . .

RUN ln -s /app/startup /usr/local/bin/air

CMD ["air"]
