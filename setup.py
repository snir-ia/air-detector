import os
import sys
from setuptools import find_packages, setup

version = '1.0.0'
name = 'air-detect'
current_dir = os.path.abspath(
    os.path.dirname(__file__)
)

if __name__ == '__main__':
    if len(sys.argv) == 2:
        if sys.argv[1] == 'name':
            print(name)
            exit()
        if sys.argv[1] == 'version':
            print(version)
            exit()

    setup(
        name=name,
        version=version
    )
