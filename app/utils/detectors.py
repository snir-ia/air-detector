import GPUtil

from app.utils.logger import logger
from .config import get_config

config = get_config()
models_path = config['PATH_TO_MODELS']
models_config = config['MODELS_CONFIG']


def get_available_gpu():
    """
    This is a bad way to get GPU for detectors. Each detector has a different
    requirements for GPU and video RAM. Instead of this for each detector an
    index of GPU must be set via configuration according runtime environment.
    """
    gpu_ids = GPUtil.getFirstAvailable(maxMemory=0.9, maxLoad=0.9, attempts=3, verbose=True, includeNan=True)
    #gpu_ids = GPUtil.getFirstAvailable(maxMemory=0.9, maxLoad=0.9)
    
    if gpu_ids == []:
        GPUtil.showUtilization()
        raise Exception('No available gpu found.')
    logger.warning("everything is OK with GPU")
    return gpu_ids[0]


def get_pushback_detector():
    from iacvcore.pushback.pushback_detector import IntellActPushbackDetector

    available_gpu = get_available_gpu()

    logger.info(f'Initializing pushback detector on gpu={available_gpu}.')

    model = IntellActPushbackDetector(config['PATH_TO_MODELS'], available_gpu)
    logger.info(f"Model {type(model)} loaded.")
    return model


def get_ramp_objects_detector():
    from iacvcore.objects_v2.object_detector_ramp import IntellactObjectDetectorRamp

    available_gpu = get_available_gpu()
    model = IntellactObjectDetectorRamp(models_root=models_path,gpu_id=available_gpu, custom_config=models_config.get("ramp"))
    logger.info(f"Model {type(model)} loaded on gpu {available_gpu}.")
    return model


def get_keypoints_detector():
    from iacvcore.keypoints.aircraft_keypoints_detector import AircraftKeypointsDetector

    available_gpu = None #get_available_gpu()
    model = AircraftKeypointsDetector(models_path,
                                      models_config.get("keypoints_model"),
                                      available_gpu)
    logger.info(f"Model {type(model)} loaded on gpu {available_gpu}.")
    return model


def get_vest_classifier():
    from iacvcore.safety_vest.vest_classifier import VestClassifier

    available_gpu = get_available_gpu()
    model = VestClassifier(models_path, available_gpu)
    logger.info(f"Model {type(model)} loaded on gpu {available_gpu}.")
    return model


def get_descriptor_extractor():
    from ia_tracker import Extractor

    model_path = f'{models_path}/{models_config["tracker_model_file"]}'
    nr_classes = 48
    model = Extractor(model_path, nr_classes, model='Resnet18', use_cuda=True)
    logger.info(f"Model {type(model)} loaded.")
    return model


def get_face_anonymizer():
    from ia_faceblur.core import FaceAnonymizer as _FaceAnonymizer

    class FaceAnonymizer(_FaceAnonymizer):
        def process_image(self, image_array, mode=None):
            super().process_image(image_array, mode)
            return self._FaceAnonymizer__result

    model = FaceAnonymizer(models_path, get_available_gpu())
    logger.info(f"Model {type(model)} loaded.")
    return model


def build_tracker(bbox_extension, objects_names):
    from ia_tracker import IntellActTracker

    model = IntellActTracker(
        max_iou_distance=0.99,
        max_age=20,
        nn_budget=10,
        objects_names=objects_names,
        bbox_extension=bbox_extension
    )
    return model


def get_object_trackers():
    from ia_tracker import objects_intervals

    objects_trackers = [(build_tracker(tracker_info['expansion'], tracker_info['objects']),
                         tracker_info['interval'])
                        for tracker_info in objects_intervals]
    return objects_trackers


def create_clearance_zone(camera_params=None):
    from ia_clearance_zone.clearance_zone import IAClearanceZone

    return IAClearanceZone(camera_params)


def create_monitoring_zone(*args, **kwargs):
    from ia_monitored_zone.aircraft_monitoring import IntellActAircraftMonitoring

    return IntellActAircraftMonitoring(*args, **kwargs)
