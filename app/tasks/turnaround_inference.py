import multiprocessing

from app.services.celery_apps import unit_celery_app, TURNAROUND_INFERENCE_TASK
from app.tasks.inferences_funcs import process_inference, mask_frame
from app.utils.detectors import get_ramp_objects_detector
from app.utils.redis import TURNAROUND_INFERENCE_HASH, cache, CAMERAS_HASH
from app.utils.logger import logger
from app.utils.config import get_config
from app.utils.turnaround import TurnaroundHelper

config = get_config()
unit_queues_config = config['SITE_UNIT_RABBITMQ_QUEUES']

ramp_objects_detector = get_ramp_objects_detector()

if multiprocessing.get_start_method(allow_none=True) is None:
    multiprocessing.set_start_method('spawn')


@unit_celery_app.task(name=TURNAROUND_INFERENCE_TASK, queue=unit_queues_config['TURNAROUND_INFERENCE_QUEUE'])
def get_turnaround_inference(camera_id):
    logger.info(f'Received turnaround task for camera {camera_id}.')

    frame, frame_ts, frame_path = TurnaroundHelper.get_frame(camera_id)

    camera_info = cache.hget(CAMERAS_HASH, camera_id)
    bounding_box = camera_info.get('mask')
    if bounding_box is not None:
        logger.debug(f'masking frame from camera {camera_id} with bounding box {bounding_box}')
        frame = mask_frame(frame, bounding_box, 127, camera_id)
    else:
        logger.debug(f'no mask for camera {camera_id}')

    inference = ramp_objects_detector.detect(frame, detect_superclasses_only=False)
    
    
    logger.debug(f'Camera {camera_id}: Inference generated: {inference}')
    logger.info(f'Camera {camera_id}: Getting turnaround inference took time: {inference["timers"]}.')

    if inference.get('error') is not None:
        logger.error(f'Error getting turnaround inference: {inference["error"]}.')

    detected_objects = process_inference(inference, frame, f'trackers_state:camera_{camera_id}', camera_id)
    if not detected_objects or len(detected_objects.get('detected_objects', [])) == 0:
        return

    cache.hset(TURNAROUND_INFERENCE_HASH, camera_id, dict(
        inference=detected_objects,
        timestamp=frame_ts,
        image_path=frame_path,
        image_width=frame.shape[1],
        image_height=frame.shape[0],
    ))
    logger.info(f"Camera {camera_id} inference processing finished.")
    return
