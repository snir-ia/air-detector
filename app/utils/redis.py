import json
from functools import wraps
from itertools import chain
from logging import getLogger
from time import time

from redis import Redis
from redis.client import Pipeline

from app.utils.logger import logger as _logger
from app.utils.config import get_config

config = get_config()
redis_url = config['SITE_MEMO_REDIS']
logger = getLogger('ia-utils.redis.' + _logger.name.rpartition('.')[-1])

FIDS_HASH = 'unit:fids'
TURNAROUND_HASH = 'unit:turnarounds'
GATES_HASH = 'unit:gates'
CAMERAS_HASH = 'unit:cameras'
TURNAROUND_INFERENCE_HASH = 'cameras:turnaround_inference'
PUSHBACK_INFERENCE_HASH = 'cameras:pushback_inference'
MONITORING_STATE_HASH = 'monitoring_states'
CAMERA_PARAMS_HASH = 'cctv-params'
AIRCRAFT_TYPES = 'aircraft-types'
FRAMES_HASH = 'unit:cameras:last_frame'
ACTIVE_TURNAROUNDS_HASH = 'unit:gates:active-turnarounds'


class PatchedRedis(Redis):
    """
    This helper converts string values got from redis into lists and dicts
    """
    _patched = False

    _valid_data_types = str, bytes, int, float
    _iterable_types = list, tuple, set, frozenset
    _mapping_types = dict,

    _plural = (
        'args',
        'values',
        'sources',
        'ids',
        'names',
    )
    remove_key_funcs = {
        'h': 'hdel',
    }

    def __new__(cls, *args, **kwargs):
        if not cls._patched:
            cls._patch()
            cls._patched = True
        return super().__new__(cls)

    def __init__(self, *args, pipeline=None, **kwargs):
        super().__init__(*args, **kwargs)
        self._pipeline = pipeline or PatchedRedisPipeline

    @classmethod
    def _patch(cls):
        """
        Subclasses doesn't have to patch again same
        routines. They inherit the patched functions.

        If subclass needs to be patched to add additional
        functionality, it must have implicitly set the
        `cls._patched = False`.
        To retrieve all the functions that been patched
        in the superclass you can use:
        ` list(set(chain(*super()._functions_to_patch().values()))) `
        """
        for decorator, funcs in cls._functions_to_patch().items():
            for name in funcs:
                setattr(cls, name, decorator(getattr(cls, name)))

    @classmethod
    def _functions_to_patch(cls):
        return {
            cls._patch_get_routine: [
                'hget',
                'hgetall',
                'hmget',
                'hvals',
                'hkeys',
                'get',
                'smembers',
                'lrange',
            ],
            cls._patch_set_routine: [
                'set',
                'hset',
                'hsetnx',
                'hmset',
                'sadd',
            ],
            cls._patch_logging_routine: [
                'type',
            ],
        }

    @staticmethod
    def _patch_logging_routine(func):
        """
        Adds following functionality:
        Logs the query to redis.
        Logs the results got.
        On exceptions - logs the exception with the query.
        """

        @wraps(func)
        def wrap(self, *args, **kwargs):
            func_repr = self.func_repr(func, *args, **kwargs)
            try:
                start = time()
                result = func(self, *args, **kwargs)
                self.query_log(func_repr, result, start)
                return result
            except Exception as e:
                logger.exception(f"Raised {e}({e.args}) When tried to queue {func_repr}")
                raise
        return wrap

    @staticmethod
    def _patch_get_routine(func):
        """
        Adds following functionality:
        Transforms all received strings data
        to json-like objects.
        If the object isn't transformable
        will remain it as is.
        """

        @PatchedRedis._patch_logging_routine
        @wraps(func)
        def wrap(self, *args, **kwargs):
            result = func(self, *args, **kwargs)
            return self.convert_result(result)
        return wrap

    @staticmethod
    def _patch_set_routine(func):
        """
        Adds following functionality:
        Will transform any json-like objects to string
        before sending to redis.
        If not transformable, will save as is.
        """

        @PatchedRedis._patch_logging_routine
        @wraps(func)
        def wrap(self, *args, **kwargs):
            var_names, arbitrary_args = self._get_func_var_names_and_arbitrary_args(func, *args)
            if arbitrary_args:
                # The function gets arbitrary *args,
                # Get the arbitrary args to convert to json
                arbitrary_args = tuple(self._dumps(value) for value in arbitrary_args[1])
                result = func(self, *args[:len(var_names)], *arbitrary_args, **kwargs)

            else:
                args = list(args)
                nargs = {name: value for name, value in chain(zip(var_names, args), kwargs.items())}

                name, key, value, mapping = (nargs.get(k) for k in ('name', 'key', 'value', 'mapping'))

                if mapping:
                    # The function gets mapping of keys-values

                    # Remove all fields that set to None
                    to_remove = [k for k, v in mapping.items() if v is None]
                    if to_remove:
                        for k in to_remove:
                            del mapping[k]

                        self.hdel(name, *to_remove)

                    # Convert the mapping to json
                    nargs['mapping'] = {k: self._dumps(v) for k, v in nargs['mapping'].items()}

                elif value is not None:
                    # The func gets only 1 value
                    # Convert it to json
                    nargs['value'] = self._dumps(nargs['value'])

                elif key and value is None and func.__name__.startswith('h'):
                    # Value of 'None' means remove the key from redis.
                    return self.hdel(name, key)

                else:
                    logger.warning(f"Got unknown function signature: {self.func_repr(func, *args, **kwargs)}")

                result = func(self, **nargs)

            return result
        return wrap

    def query_log(self, func_repr, result, started):
        res = str(result)
        if len(res) > 1000:
            res = f"{res[:300]} ... {res[-300:]}"
        logger.debug(f"Command {func_repr} ran for {round(time() - started, 4)}. "
                     f"Resulted: {res}")

    def func_repr(self, func, *args, **kwargs):
        """
        Generate string with func representation
        func name and all arguments key-value pairs.
        """
        var_names, arbitrary_args = self._get_func_var_names_and_arbitrary_args(func, *args)
        if arbitrary_args:
            params = chain(zip(var_names, args), zip(*arbitrary_args), kwargs.items())
        else:
            params = chain(zip(var_names, args), kwargs.items())
        return f"{func.__name__}{tuple('='.join(str(p) for p in param) for param in params)}"

    def _get_func_var_names_and_arbitrary_args(self, func, *args):
        """
        Find the names of positional arguments of the func
        and the arguments that are plural (ie. *keys).
        """
        var_names = func.__code__.co_varnames[1:]
        arbitrary_args = [arg for arg in self._plural if arg in var_names]

        if not arbitrary_args or len(args) < len(var_names):
            return var_names, None

        else:
            pos_args_ends = var_names.index(arbitrary_args[0])
            return var_names[:pos_args_ends], (arbitrary_args[0], args[pos_args_ends:])

    def convert_result(self, result):
        """
        The result type depends on redis structure:
        hash -> dict,
        list -> list,
        set -> set,
        str -> str,
        int -> int,
        float -> float.

        This converter will check the type of result and
        keep it as is. But the value/s will be tried
        to get converted to json object.
        """
        RedisResultType = type(result)

        if RedisResultType in self._iterable_types:
            # The result type (got from redis) is list or set
            try:
                return RedisResultType(self._loads(r) for r in result if r)
            except TypeError:
                # todo: check why SOME sets returned in this weird way
                return RedisResultType(*(self._loads(r) for r in result if r))

        elif RedisResultType in self._mapping_types:
            # The result type is hash - dict
            return RedisResultType({k: self._loads(v) for k, v in result.items()})

        elif not result:
            return None

        else:
            # The result is single value
            return self._loads(result)

    def _dumps(self, data):
        if not isinstance(data, self._valid_data_types):
            data = json.dumps(data)
        return data

    @staticmethod
    def _loads(data):
        try:
            data = json.loads(data)
        except:
            pass
        return data

    def pipeline(self, transaction=True, shard_hint=None):
        """
        This only differs from the original function by accessing definable Pipeline class
        """
        return self._pipeline(
            self.connection_pool,
            self.response_callbacks,
            transaction,
            shard_hint)


class PatchedRedisPipeline(PatchedRedis, Pipeline):
    _patched = False

    max_command_stack_size = 20

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._query_logs = {}
        self._partial_result = []

    @classmethod
    def _functions_to_patch(cls):
        if not cls.max_command_stack_size:
            # When max_command_stack_size = 0
            # the command_stack is unlimited
            max_stack_funcs = []
        else:
            # If some function will not be patched
            # by max_command_stack_execution, it will not
            # count towards the final amount, therefore
            # the command_stack could be longer.
            max_stack_funcs = list(set(chain(*super()._functions_to_patch().values())))

        return {
            cls._patch_max_pipe_stack_size_execution: max_stack_funcs,
        }

    @staticmethod
    def _patch_max_pipe_stack_size_execution(func):
        @wraps(func)
        def wrap(self, *args, **kwargs):
            result = func(self, *args, **kwargs)
            if len(self.command_stack) >= self.max_command_stack_size:
                self._partial_result = self.execute()
            return result

        return wrap

    def execute(self, raise_on_error=True):
        """
        After the execution this will log down
        all the pipelined commands and their results.
        Note! This only logs the commands and results
        that would be logged by redis patched.
        """
        try:
            started = time()
            results = [self.convert_result(res) for res in super().execute(raise_on_error)]
            logs = (f'{self._query_logs[cmd_idx]} := {res}' for cmd_idx, cmd, res in
                    zip(range(len(self.command_stack)), self.command_stack, results)
                    if cmd_idx in self._query_logs)
            logger.debug(f"Pipelined commands took {round(time() - started, 4)}seconds: "
                         f"{' ;; '.join(logs)}")
            if self._partial_result:
                return self._partial_result + results
            return results

        except Exception as e:
            logger.error(f"Raised {e}. When tried to pipeline commands: "
                         f"{' ; '.join(self._query_logs)}")
            raise

        finally:
            self._query_logs.clear()

    def query_log(self, func_repr, result, started):
        self._query_logs[len(self.command_stack)] = func_repr


def update_aircraft_types(path):
    """
    This is for manual update of aircraft types from a file.
    """
    with open(path) as f:
        data = json.load(f)

    existing = cache.hgetall(AIRCRAFT_TYPES)
    if any(k not in existing for k in data):
        existing.update({k: v for k, v in data.items() if k not in existing})
        cache.hset(AIRCRAFT_TYPES, mapping=existing)


cache = PatchedRedis.from_url(config['SITE_MEMO_REDIS'], decode_responses=True)
