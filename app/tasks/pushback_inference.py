import multiprocessing
import time

from app.services.celery_apps import unit_celery_app, PUSHBACK_INFERENCE_TASK
from app.tasks.inferences_funcs import get_frame, process_inference
from app.utils.detectors import get_pushback_detector
from app.utils.redis import PUSHBACK_INFERENCE_HASH, cache
from app.utils.logger import logger
from app.utils.config import get_config

config = get_config()
unit_queues_config = config['SITE_UNIT_RABBITMQ_QUEUES']

pushback_detector = get_pushback_detector()

if multiprocessing.get_start_method(allow_none=True) is None:
    multiprocessing.set_start_method('spawn')


@unit_celery_app.task(name=PUSHBACK_INFERENCE_TASK, queue=unit_queues_config['PUSHBACK_INFERENCE_QUEUE'])
def get_pushback_inference(camera_id):
    logger.info(f'Received pushback task for camera {camera_id}.')

    frame, frame_ts, frame_path = get_frame(camera_id)
    start = time.time()
    inference = pushback_detector.detect_objects(frame)
    logger.debug(f'Getting pushback inference took time: {time.time() - start}')

    detected_objects = process_inference(inference, frame, f'trackers_state:pushback:camera_{camera_id}', camera_id)
    if not detected_objects:
        return

    cache.hset(PUSHBACK_INFERENCE_HASH, camera_id, dict(
        inference=detected_objects,
        timestamp=frame_ts,
        image_path=frame_path,
        image_width=frame.shape[1],
        image_height=frame.shape[0],
    ))
