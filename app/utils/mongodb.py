import logging
from pymongo import MongoClient as _MongoClient
from app.utils.config import get_config

config = get_config()
logger = logging.getLogger("mongodb")


class MongoClient:

    def __init__(self, uri):
        logger.debug("connecting to mongodb")
        self.client = _MongoClient(uri, maxPoolSize=100, connect=True)
        self.database = self.client.get_database()
        self.events_collection = self.database.get_collection("events")
        self.fids_collection = self.database.get_collection("fids")
        self.turnaround_collection = self.database.get_collection("turnarounds")
        logger.debug(f"mongodb is now connected")

    def save_fids(self, fids_record):
        insert_result = self.fids_collection.insert_one(fids_record)
        logger.debug(f"new fids saved: {fids_record}")
        return insert_result


complete_uri = config["MONGODB_URI"]
mongodb = MongoClient(complete_uri)
