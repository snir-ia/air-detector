import time

import cv2
import numpy as np

from app.utils.config import get_config
from app.utils.detectors import (
    get_descriptor_extractor,
    get_keypoints_detector,
    get_object_trackers,
    get_vest_classifier
)
from app.utils.logger import logger
from app.utils.redis import cache
from app.utils.utils import to_floats

config = get_config()

extractor = get_descriptor_extractor()
keypoints_detector = get_keypoints_detector()
vest_classifier = get_vest_classifier()


def filter_confidence(detected_objects, conf_threshold):
    """Filter the detected objects according to confidence threshold.

    Arguments:
        detected_objects (list): List of detected objects
        conf_threshold (float): Detection confidence threshold
    Returns:
        (list): List of detected objects with more confidence than threshold
    """
    # todo: basically the threshold should be different for superclass-detection and subclass-classification
    filtered_objects = [obj for obj in detected_objects
                        if obj.get('subclass_prob') or obj.get('superclass_confidence') >= conf_threshold]
    return filtered_objects


def process_inference(inference, image, tracks_state_redis_key, camera_id):
    detected_objects = inference['detections']
    for obj in detected_objects:
        if 'bbox' in obj:
            obj['bbox'] = [int(n) for n in obj['bbox']]

    inject_features_descriptor(detected_objects, image)

    tracked_objects = get_tracked_objects(detected_objects, tracks_state_redis_key, camera_id)
    if not tracked_objects:
        logger.info(f'Camera {camera_id}: Nothing to track. Skipping..')
        return

    # todo: check why detected aircrafts and keypoints got shuffled

    aircrafts_keypoints = detect_aircrafts_keypoints(image, detected_objects, camera_id)
    detect_vests(image, tracked_objects, camera_id)
    return {'detected_objects': tracked_objects, 'keypoints': aircrafts_keypoints}


def inject_features_descriptor(detected_objects, frame):
    bboxes = [np.int32(det_obj['bbox']) for det_obj in detected_objects]
    features = extractor(bboxes, frame)

    for det_object, feature in zip(detected_objects, features):
        det_object['feature'] = feature


def get_tracked_objects(detected_objects, tracks_state_redis_key, camera_id):
    """
    The tracker uses the feature descriptor
    to compare to objects in previous inference.
    If the descriptor equals to previously
    detected object, it will assign the proper
    track_id. Else - assign new track_id.
    """
    object_trackers = get_object_trackers()
    load_states_of_tracked_objects(object_trackers, tracks_state_redis_key, camera_id)
    tracked_objects = update_tracked_objects(detected_objects, object_trackers, camera_id)
    cache.set(tracks_state_redis_key, [tracker.save_state() for tracker, _ in object_trackers])
    return tracked_objects


def load_states_of_tracked_objects(object_trackers, redis_key, camera_id):
    start_time = time.time()
    trackers_state = cache.get(redis_key)
    if trackers_state:
        for (tracker, _), state in zip(object_trackers, trackers_state):
            tracker.load_state(state)
        logger.info(f'Camera {camera_id}: Loading trackers state: {time.time() - start_time}')


def update_tracked_objects(detected_objects, object_trackers, camera_id):
    """
    This will reload all the tracked objects
    even the once that didn't updated
    in order to get their current status.
    """
    start_time = time.time()
    tracked_objects = []
    for tracker, _ in object_trackers:
        tracker.update(detected_objects)
        tracked_objects.extend(tracker.get_tracks())

    logger.info(f'Camera {camera_id}: Tracking took time: {time.time() - start_time}')
    return tracked_objects


def detect_aircrafts_keypoints(image, detected_objects, camera_id):
    start = time.time()
    aircrafts = [obj for obj in detected_objects if obj.get('superclass_name') == 'aircraft']
    if not aircrafts:
        return

    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    aircrafts_keypoints = keypoints_detector.get_points(image, aircrafts)
    if aircrafts_keypoints:
        aircrafts_keypoints = to_floats(aircrafts_keypoints)
    logger.debug(f'Camera {camera_id}: Getting aircrafts keypoints took time: {time.time() - start}')
    return aircrafts_keypoints


def detect_vests(image: np.array, tracked_objects, camera_id: int):
    start = time.time()
    objects = [obj for obj in tracked_objects if obj.get('superclass_name') == 'person']
    bboxes = [obj['bbox'] for obj in objects]
    images = [image[bbox[1]:bbox[3], bbox[0]:bbox[2]] for bbox in bboxes]
    for obj, has_vest in zip(objects, vest_classifier.is_vest_detected_batch(images)):
        obj['has_vest'] = has_vest
    logger.debug(f"Camera {camera_id}: Finding vests took time: {time.time() - start}")


def mask_frame(frame, bounding_box, color, camera_id):
    #bounding box is [x1,y1,x2,y2]
    mask = create_mask_from_roi(bounding_box, frame.shape)
    masked_image = apply_mask(frame, mask)
    #save_path = config['APP_HOME_DIR'] + f'/{camera_id}__{datetime.datetime.now().isoformat()}.jpg'
    #cv2.imwrite(save_path, masked_image)
    return masked_image
    # start_point = (bounding_box[0], bounding_box[1])
    # end_point = (bounding_box[2], bounding_box[3])
    # return cv2.rectangle(frame, start_point, end_point, (color, color, color), -1)


def create_mask_from_roi(bbox, shape):
    mask = np.zeros((shape[0], shape[1], 3), dtype="uint8")
    x1, y1, x2, y2 = bbox
    mask[y1:y2+1, x1:x2+1, :] = 255
    return mask


def apply_mask(frame, mask):  # Hide some objects, from other gates!
    masked_frame = np.copy(frame)
    mask_index = mask == 0
    masked_frame[mask_index] = 0
    return masked_frame
