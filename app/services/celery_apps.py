import uuid

from celery import Celery, signals
from kombu import Exchange, Queue

from ia_utils.celery.site_unit_token import AMQPWithTokenHeader

from app.utils.config import get_config

config = get_config()
unit_queues_config = config['SITE_UNIT_RABBITMQ_QUEUES']

unit_celery_app = Celery(**config['SITE_UNIT_RABBITMQ'], backend='rpc')
hub_celery_app = Celery(amqp=AMQPWithTokenHeader(config['SITE_UNIT_TOKEN']), **config['HUB_RABBITMQ'], backend='rpc')


@signals.celeryd_after_setup.connect
def init_app(sender, instance, **kwargs):
    '''Declare exchange for processing detected events/alerts'''
    with instance.app.producer_or_acquire() as producer:
        channel = producer.channel
        exchange = Exchange(config['EVENT_PROCESSING_EXCHANGE'],
                            type='fanout',
                            durable=True,
                            channel=channel)
        exchange.declare()


# Unit Tasks definition
AIR_SCHEDULER_TASK = 'tasks.air_scheduler'
SAFETY_WRAPPER_TASK = 'tasks.air_safety'
TURNAROUND_UPDATE_TASK = 'tasks.status_update'
WATCHER_NORMAL_TASK = 'tasks.run_watcher:normal'
WATCHER_COUNTING_TASK = 'tasks.run_watcher:counting'
SPAWN_WATCHER_TASK = 'tasks.spawn_watcher'
ESTIMATE_CAMERA_PARAMS_TASK = 'tasks.estimate_camera_params'
CAMERA_MOVEMENT_DETECTION_TASK = 'tasks.detect_movement'
TURNAROUND_INFERENCE_TASK = 'tasks.turnaround_inference'
PUSHBACK_INFERENCE_TASK = 'tasks.pushback_inference'
SPAWN_WATCHERS_MANAGER_TASK = 'tasks.spawn_turnaround_watchers'

# Hub Tasks definition
CREATE_FIDS_TASK = 'hub.create_fids'
UPDATE_FIDS_TASK = 'hub.update_fids'
UPDATE_CAMERA_TASK = 'hub.change_camera_mode'

# Unit queues definition
APP_QUEUE = f'{config["AIRPORT_IATA"]}_{config["APP_QUEUE"]}'
watchers_dedicated_queue = Queue(f'{APP_QUEUE}_{uuid.uuid4()}', durable=False,
                                 auto_delete=True, message_ttl=1 * 6000)

unit_celery_app.conf.task_routes = {
    AIR_SCHEDULER_TASK: {'queue': unit_queues_config['AIR_SCHEDULER_QUEUE']},
    SAFETY_WRAPPER_TASK: {'queue': unit_queues_config['SAFETY_WRAPPER_QUEUE']},
    WATCHER_NORMAL_TASK: {'queue': unit_queues_config['AGGREGATOR_QUEUE']},
    WATCHER_COUNTING_TASK: {'queue': unit_queues_config['AGGREGATOR_QUEUE']},
    SPAWN_WATCHER_TASK: {'queue': unit_queues_config['MANAGER_QUEUE']},
    CAMERA_MOVEMENT_DETECTION_TASK: {'queue': unit_queues_config['CAMERA_MOVEMENT_QUEUE']},
    TURNAROUND_INFERENCE_TASK: {'queue': unit_queues_config['TURNAROUND_INFERENCE_QUEUE']},
    PUSHBACK_INFERENCE_TASK: {'queue': unit_queues_config['PUSHBACK_INFERENCE_QUEUE']},
    SPAWN_WATCHERS_MANAGER_TASK: {'queue': watchers_dedicated_queue},
}
