from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta
from enum import Enum
from pathlib import Path
from threading import Thread
from time import sleep, time

import numpy as np

from app.utils.logger import logger
from app.utils.redis import (
    AIRCRAFT_TYPES, FIDS_HASH, FRAMES_HASH, GATES_HASH, MONITORING_STATE_HASH, TURNAROUND_HASH,
    ACTIVE_TURNAROUNDS_HASH, cache)
from app.utils.config import get_config
from app.utils.utils import dump_frame
from app.utils.mongodb import mongodb

config = get_config()

OBJECTS_HEIGHTS = {
    'wheel-chock': {
        'height': 0.15,
        'altitude': 0.0
    },
    'wheel-chock-carrier': {
        'height': 0.15,
        'altitude': 0.0
    },
    'traffic-bollard': {
        'height': 1.0,
        'altitude': 0.0
    },
    'cone': {
        'height': 0.5,
        'altitude': 0.0
    },
    'flight-attendant': {
        'height': 1.7,
        'altitude': 0.0
    },
    'ramp-personal': {
        'height': 1.7,
        'altitude': 0.0
    },
    'bus': {
        'height': 3.0,
        'altitude': 0.0,
    },
    'tractor': {
        'height': 1.8,
        'altitude': 0.0,
    },
    'dolly': {
        'height': 0.3,
        'altitude': 0.0
    },
    'pilot': {
        'height': 1.7,
        'altitude': 0.0
    },
    'ground-power-unit-connected': {
        'height': 0.5,
        'altitude': 0.0
    },
    'ground-power-unit-disconnected': {
        'height': 0.5,
        'altitude': 0.0
    },
    'pre-conditioned-air-pipe-connected': {
        'height': 0.5,
        'altitude': 0.0
    },
    'pre-conditioned-air-pipe-disconnected': {
        'height': 0.5,
        'altitude': 0.0
    },
    'jet-bridge': {
        'height': 4.0,
        'altitude': 0.0,
    },
    'jet-bridge-attached': {
        'height': 4.0,
        'altitude': 0.0,
    },
    'stairs-driving': {
        'height': 2.5,
        'altitude': 0.0,
    },
    'stairs-attached': {
        'height': 2.5,
        'altitude': 0.0,
    },
    'passenger': {
        'height': 1.7,
        'altitude': 0.0
    },
    'service-door': {
        'height': 3.0,
        'altitude': 0.0
    },
    'water-truck-driving': {
        'height': 1.5,
        'altitude': 0.0
    },
    'water-truck-operation': {
        'height': 1.5,
        'altitude': 0.0
    },
    'fuel-truck-driving': {
        'height': 2.0,
        'altitude': 0.0
    },
    'fuel-truck-operation': {
        'height': 2.0,
        'altitude': 0.0
    },
    'catering-vehicle-driving': {
        'height': 2.6,
        'altitude': 0.0
    },
    'catering-vehicle-operation': {
        'height': 5.5,
        'altitude': 0.0
    },
    'ambulance-truck-driving': {
        'height': 2.6,
        'altitude': 0.0
    },
    'ambulance-truck-operation': {
        'height': 5.5,
        'altitude': 0.0
    },
    'cleaning-staff': {
        'height': 1.7,
        'altitude': 0.0
    },
    'cargo-door': {
        'height': 1.5,
        'altitude': 1.0
    },
    'fire-truck': {
        'height': 3.0,
        'altitude': 0.0
    },
    'garbage-vehicle': {
        'height': 3.0,
        'altitude': 0.0
    },
    'ramp-cleaning-vehicle': {
        'height': 2.0,
        'altitude': 0.0
    },
    'cargo-vehicle-driving': {
        'height': 1.5,
        'altitude': 0.0
    },
    'cargo-vehicle-operation': {
        'height': 1.5,
        'altitude': 0.0
    },
    'baggage-cart': {
        'height': 2.0,
        'altitude': 0.0
    },
    'cargo-piece': {
        'height': 0.5,
        'altitude': 0.0
    },
    'luggage': {
        'height': 0.5,
        'altitude': 0.0
    },
    'container-on-belt': {
        'height': 0.5,
        'altitude': 0.0
    },
    'container-not-on-belt': {
        'height': 0.5,
        'altitude': 0.0
    },
    'pallet-on-belt': {
        'height': 0.5,
        'altitude': 0.0
    },
    'pallet-not-on-belt': {
        'height': 0.5,
        'altitude': 0.0
    },
    'pushback-truck-driving': {
        'height': 2.0,
        'altitude': 0.0
    },
    'pushback-truck-attached': {
        'height': 2.0,
        'altitude': 0.0
    },
    'service-vehicle': {
        'height': 2.0,
        'altitude': 0.0
    },
    'ladder': {
        'height': 1.5,
        'altitude': 0.0
    },
    'lamp': {
        'height': 5.0,
        'altitude': 0.0
    },
    'pushback-connector': {
        'height': 0.4,
        'altitude': 0.0
    },
    'person': {
        'height': 1.7,
        'altitude': 0.0
    },
    'vehicle': {
        'height': 2.0,
        'altitude': 0.0
    },
    'object': {
        'height': 0.5,
        'altitude': 0.0
    },
    'structure': {
        'height': 3.5,
        'altitude': 0.0
    }
}


class NoTurnaroundsError(Exception):
    pass


class TurnaroundStatus(Enum):
    SCANNER = 'scanner'
    AWAITING = 'awaiting'
    TURNAROUND = 'turnaround'
    IN = 'in'
    IDLE = 'idle'
    OUT = 'out'
    PUSHBACK = 'pushback'
    DONE = 'done'

    def __str__(self):
        return self.value

    @property
    def frame_rate(self):
        conf = config['PROCESS_RATE']
        return conf.get(self.value.upper(), conf['SCANNER'])


class TurnaroundType(Enum):
    QUICK = "quick"
    NORMAL = "normal"
    DOMESTIC = "domestic"
    INTERNATIONAL = "international"


# todo: turns out, this can't be mocked,
#  which causes on querying redis every minute,
#  which is annoying on local tests
class CommonDataUpdater(Thread):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start()

    def run(self):
        to_sleep = 60 #config['UPDATE_COMMON_DATA_INTERVAL']
        while True:
            try:
                logger.debug(f"Updating essential data from redis.")
                TurnaroundHelper.fetch_active_gates()
                TurnaroundHelper.fetch_aircraft_types()
                sleep(to_sleep)
            except:
                sleep(0.001)


class TurnaroundHelper(dict):
    _updater = CommonDataUpdater()
    _frame_dumper = ThreadPoolExecutor(max_workers=1)
    active_gates = None
    aircraft_types = None
    
    def __new__(cls, turnaround=None):
        if isinstance(turnaround, TurnaroundHelper):
            # Avoid copying dict during creation
            return turnaround
        return super().__new__(cls)

    def __init__(self, turnaround=None):
        """
            turnaround structure: {
                "id": id,
                "status": "scanner" | "turnaround" | "pushback",
                "mode": "active" | "onboarding" | "fixes" | "disabled", - not sure if we need this here
                "stage_in_ts": timestamp,
                "stage_out_ts": timestamp,
                "turn_type": "turn_type",
                "sta_utc": timestamp,
                "std_utc": timestamp,
                "arrival_flight_number": 123,
                "departure_flight_number": 456,
                "incoming_gate": "A1",
                "outgoing_gate": "A1",
                "parking_gate": "A1",
            }
        """
        super().__init__(turnaround or {})

        self._cameras = None
        self._timestamp_key = f"turnaround_{self.id}_timestamp"
        self._status_key = f"turnaround_{self.id}_status"

        self._sta = None
        self._ata = None
        self._std = None
        self._atd = None
        self._status = None

    def __gt__(self, other):
        return self['sta_utc'] > other['sta_utc']

    def __lt__(self, other):
        return self['sta_utc'] < other['sta_utc']

    # Properties
    @property
    def id(self):
        if not self:
            return None
        return self['id']

    @id.setter
    def id(self, value):
        self['id'] = value

    @property
    def sta(self):
        if not self._sta and self['sta_utc']:
            self._sta = datetime.fromisoformat(self['sta_utc'])
        return self._sta

    @property
    def ata(self):
        if not self._ata and self['ata_utc']:
            self._ata = datetime.fromisoformat(self['ata_utc'])
        return self._ata

    @property
    def std(self):
        if not self._std and self['std_utc']:
            self._std = datetime.fromisoformat(self['std_utc'])
        return self._std

    @property
    def atd(self):
        if not self._atd and self['atd_utc']:
            self._atd = datetime.fromisoformat(self['atd_utc'])
        return self._atd

    @property
    def unique_key(self):
        if not self:
            return None
        return f"{self.sta.date()};{self['arrival_flight_number']};{self['departure_flight_number']}"

    @property
    def gates(self):
        gates = (self.get(gate) for gate in ('incoming_gate', 'outgoing_gate', 'parking_gate'))
        return {gate for gate in gates if gate}

    @property
    def cameras(self):
        if not self.gates:
            return None
        if not self._cameras:
            self._cameras = set().union(*cache.hmget(GATES_HASH, self.gates))
        return self._cameras

    @property
    def last_monitored(self):
        return cache.get(self._timestamp_key)

    @last_monitored.setter
    def last_monitored(self, value):
        cache.set(self._timestamp_key, value)

    @property
    def status(self):
        if not self:
            return TurnaroundStatus.SCANNER

        if not self._status:
            self._status = cache.get(self._status_key)

        if not self._status:
            logger.info(f'No status exists yet. Setting {self.id} status to scanner.')
            # This puts the new status to redis
            self.status = 'scanner'

        return TurnaroundStatus(self._status)

    @status.setter
    def status(self, value):
        logger.info(f'TurnaroundHelper {self.id} changed status to {value}.')
        cache.set(self._status_key, value)
        if isinstance(value, TurnaroundStatus):
            value = value.value
        self._status = value

    @property
    def mode(self):
        return 'out' if self.get('stage_out_ts') else 'in'

    @property
    def ended(self):
        return self['atd_utc'] is not None or self.status in (TurnaroundStatus.DONE, )

    # Factories
    @classmethod
    def from_cache(cls, ids, hash_key=TURNAROUND_HASH):
        """
            Get turnarounds from redis and return them as
            (list of) TurnaroundHelper objects

        Args:
            ids: Union[str, Iterable] - one or list of keys.
                This can be any query that redis can parse
            hash_key: str - the hash in redis to look for turnarounds
                (should be `unt:turnarounds` or `unit:fids`)

        Returns: Union[list[TurnaroundHelper], TurnaroundHelper]
            single or list of TurnaroundHelper objects.
            If no ids passed (a generator of ids, that in fact is empty)
            will return empty list.
        """
        if isinstance(ids, str):
            turnaround = cache.hget(hash_key, ids)
            if turnaround:
                return cls(turnaround)
            else:
                return

        ids = tuple(ids)
        if not ids:
            return []

        return [cls(t) for t in cache.hmget(hash_key, ids) if t]

    @classmethod
    def from_fids(cls, keys):
        return cls.from_cache(keys, hash_key=FIDS_HASH)

    @classmethod
    def gate_turnarounds(cls, gate):
        return cls.from_cache(cache.smembers(cls._get_gate_key(gate)))

    @classmethod
    def get_active_turnaround(cls, gate):
        id = cache.hget(ACTIVE_TURNAROUNDS_HASH, gate)
        if not id:
            return TurnaroundHelper()
        return cls.from_cache(id)

    # Main functionality
    def cache(self):
        cache.hset(FIDS_HASH, self.unique_key, self)

        # todo: remove the patching of aircraft when becomes obsolete
        if self['aircraft_type'] not in self.aircraft_types:
            self['aircraft_type'] = '737-800'

        cache.hset(TURNAROUND_HASH, self.id, self)

    def attach(self):
        """
            Get all cameras of the gates
            Attach the turnaround to each gate and each camera
        """
        if not self.gates:
            logger.info(f"The turnaround {self.id} doesn't have any gates specified. Can't watch it.")
            return

        gates = self.gates.intersection(self.active_gates)
        if not gates:
            logger.info(f"The turnaround {self.id} runs on inactive gates. Can't watch it.")
            return

        with cache.pipeline() as pipe:
            for gate in gates:
                pipe.sadd(self._get_gate_key(gate), self.id)

            pipe.execute()

    def detach(self):
        """
            Removes the turnaround from previously
            assigned gates and cameras.
        """
        self._cameras = None
        with cache.pipeline() as pipe:
            for k in self.gates:
                pipe.srem(self._get_gate_key(k), self.id)
            pipe.execute()

    def update(self, __m=None, **kwargs):
        # Work only with one variable
        if __m:
            kwargs.update(__m)

        # Update everything except of id
        if 'id' in kwargs:
            del kwargs['id']

        if any(kwargs.get(k) != self[k] for k in ('parking_gate', 'incoming_gate', 'outgoing_gate')):
            # Detach before updating the dict
            self.detach()

        super().update(kwargs)
        self.cache()
        self.attach()

    def remove(self):
        """
            Remove turnaround from each gate related to it,
            Delete turnaround itself.
        """
        self.detach()
        mongodb.save_fids(self)
        data_keys = [k for k in cache.keys(f"*{self.id}*")]
        with cache.pipeline() as pipe:
            pipe.hdel(TURNAROUND_HASH, self.id)
            pipe.hdel(FIDS_HASH, self.unique_key)
            pipe.hdel(MONITORING_STATE_HASH, self.id)
            if data_keys:
                pipe.unlink(*data_keys)
            pipe.execute()

    # Static and Class Functionality
    @classmethod
    def fetch_active_gates(cls):
        cls.active_gates = [str(k) for k in cache.hkeys(GATES_HASH) if not isinstance(k, str) or ':' not in k]

    @classmethod
    def fetch_aircraft_types(cls):
        cls.aircraft_types = cache.hgetall(AIRCRAFT_TYPES)

    @staticmethod
    def _get_gate_key(value):
        return f"gate_{value}"

    @classmethod
    def get_camera_status(cls, camera):
        """
            Go through all the turnarounds related to this camera and are currently
            in progress and return the camera status(can be converted to the according frame rate)
            based on the turnaround statuses.

            E.g.: if process A has a turnaround status(frame rate = 3s), process B
            has a pushback status(frame rate = 1s), then camera status
            should be pushback as its the lowest rate and it will for sure
            cover the frame rate of turnaround
        """
        gates = [g.partition(':')[0] for g in cache.hkeys(GATES_HASH)
                 if isinstance(g, str) and f':{camera}' in g]
        active_turnarounds = [cls.get_active_turnaround(g) for g in gates]
        active_turnarounds = [t for t in active_turnarounds if t]
        active_turnarounds.sort(key=lambda turnaround: turnaround.status.frame_rate)
        try:
            return active_turnarounds[0].status
        except IndexError:
            logger.debug(f'No turnarounds for camera {camera}.')
            return TurnaroundStatus.SCANNER

    @staticmethod
    def attach_camera_to_gate(gate, camera):
        # todo: seems to be useless, as we can't add the camera params...
        cameras = set(cache.hget(GATES_HASH, gate))
        with cache.pipeline() as pipe:
            if camera not in cameras:
                cameras.update(camera)
                pipe.hset(GATES_HASH, gate, cameras)

            pipe.hsetnx(GATES_HASH, f"{gate}:{camera}", [])
            pipe.execute()

    @staticmethod
    def set_active_turnaround(gate, id):
        cache.hset(ACTIVE_TURNAROUNDS_HASH, gate, id)

    @classmethod
    def rearrange_active_turnarounds(cls):
        logger.info(f"Setting active turnarounds on gates.")
        now = datetime.utcnow()
        active = {}
        for gate in cls.active_gates:
            turnarounds = sorted(
                (t for t in cls.gate_turnarounds(gate)
                 if t.sta - timedelta(minutes=30) < now < t.std + timedelta(hours=12)
                 and not t.ended)
            )

            if not turnarounds:
                active[gate] = None
                continue

            started_turnarounds = [t for t in turnarounds
                                   if t['ata_utc']
                                   or t.status in (TurnaroundStatus.TURNAROUND, TurnaroundStatus.PUSHBACK)]
            if started_turnarounds:
                if len(started_turnarounds) > 1:
                    for t in sorted(started_turnarounds, key=lambda t: t.ata):
                        if now - t.std < timedelta(minutes=30):
                            break
                    logger.warning(f"Gate {gate} got more than 1 active turnarounds: "
                                   f"{', '.join(t.id for t in started_turnarounds)}. "
                                   f"Focusing on turnaround {t.id} with times: "
                                   f"sta={t['sta_utc']}, ata={t['ata_utc']}, std={t['std_utc']}.")

                else:
                    t = started_turnarounds[0]
            else:
                t = turnarounds[-1]  # closest turnaround

            active[gate] = t.id

        for gate, turnaround in active.items():
            cls.set_active_turnaround(gate, turnaround)

        logger.debug(f"Active turnarounds set to: " +
                  ', '.join(f"gate-{gate}= {turnaround}" for gate, turnaround in active.items()))

    @classmethod
    def save_frame(cls, frame, camera_id):
        start = time()
        now = datetime.utcnow()
        path = Path(
            config['APP_HOME_DIR'], config['CAMERA_IMAGES_PATH'],
            now.date().isoformat(), str(camera_id), f'{int(now.timestamp() * 1000)}.jpg')
        # todo: passing the shape is redundant: the camera frame shape should not change!
        cache.hset(f'{FRAMES_HASH}:{camera_id}', mapping={
            'ts': now.isoformat(), 'path': str(path),
            # 'frame': frame.tobytes(), 'shape': frame.shape,
        })
        feature = cls._frame_dumper.submit(dump_frame, frame, path)
        feature.result()
        logger.debug(f'Saving frame took time: {time() - start}')

    @staticmethod
    def get_frame(camera_id):
        start = time()
        frame_data = cache.hgetall(f'{FRAMES_HASH}:{camera_id}')
        frame, shape, ts, path = (frame_data.get(k) for k in ('frame', 'shape', 'ts', 'path'))

        import cv2
        frame = cv2.imread(path)
        logger.debug(f'Reading a frame took time: {time() - start}')
        return frame, ts, path

        if not all((frame, shape, path, ts)):
            logger.error(f"Reading frame has failed: Redis missing some frame data!")
            return None, None, None

        path = Path(path)
        frame = np.frombuffer(frame, np.uint8).reshape(shape)
        logger.debug(f'Reading a frame took time: {time() - start}')
        return frame, ts, path

    @staticmethod
    def clear_cache():
        """
        Removes from the cache all data related to turnarounds.
        """
        keys = cache.scan('*')
        gates = cache.hgetall(GATES_HASH)
        cameras_bound = [k for k in keys if k.startswith('turnaround_') and k.endswith('_cameras')]
        gates_bindings = [k for k in keys if k.startswith('gate_') and k.partition('_')[-1] in gates]
        cameras_bindings = [k for k in keys if k.startswtih('camera_')]
        cache.unlink(TURNAROUND_HASH, FIDS_HASH, *cameras_bound, *gates_bindings, *cameras_bindings)
